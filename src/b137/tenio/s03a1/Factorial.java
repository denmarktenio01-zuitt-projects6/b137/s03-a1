package b137.tenio.s03a1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        int factorThis;
        long factorial = 1;

        System.out.println("Enter the number:");
        factorThis = userInput.nextInt();
        if (factorThis != 0) {
            if (factorThis > 0) {
                for (int i = 1; i <= factorThis; i++){
                    factorial = factorial * i;


                }
                System.out.println("The factorial of " + factorThis + " is " + factorial);

            } else {
                System.out.println("I'm not sure if negative numbers have factorials.");

            }



        } else {
            System.out.println("The factorial of " + factorThis + " is 1");
        }






    }
}
